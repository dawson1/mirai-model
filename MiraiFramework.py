"""

Mirai data model runing  time  enviroment


Created on Tue May 24 11:26:36 2016


ArSimpleController is run a Fake model , hehe

@author: Dawson
"""

import web
from qedata.mirai.richArModel import arModel , expensesModel , prModel
from qedata.mirai.controllers.RichDataController import ArModelController,ExpensesModelController,PayrollModelController, ArSimpleController , ApSimpleController
from qedata.mirai.controllers.ArModelController import ArXmodel , ArXmodelPredAction , ArXmodelEvaluation
from qedata.mirai.controllers.ExpController import ExpCreateAction,ExpPredictAction,ExpEvaluationAction
from qedata.mirai.controllers.PayrollController import  PRCreateAction , PRPredictAction ,  PrEvaluationAction
from qedata.mirai.controllers.ApModelCOntroller import ApCreateAction , ApPredictAction ,  ApEvaluationAction

urls = (
    '/model/ar', 'ArModelController' ,
    '/model/expenses' , 'ExpensesModelController' ,
    '/model/payroll' , 'PayrollModelController' ,
    '/model/arsimple' , 'ArSimpleController' ,
    '/model/apsimple' , 'ApSimpleController' ,

    '/model/uar' , 'ArXmodel',
    '/model/uar/predict', 'ArXmodelPredAction' ,
    '/model/uar/evaluation' ,  'ArXmodelEvaluation' ,
    '/model/uexp' , 'ExpCreateAction',
    '/model/uexp/predict' , 'ExpPredictAction' ,
    '/model/uexp/evaluation' , 'ExpEvaluationAction' ,
    '/model/upr' , 'PRCreateAction' ,
    '/model/upr/predict' , 'PRPredictAction' ,
    '/model/upr/evaluation' , 'PrEvaluationAction' ,
    '/model/uap' , 'ApCreateAction',
    '/model/uap/predict' , 'ApPredictAction' ,
    '/model/uap/evaluation' ,  'ApEvaluationAction'
)


def initHandler():
    print("init  handler")


def main():
    web.config.debug = False
    app = web.application(urls, globals())
    app.run()


if __name__ == '__main__':
    main()

