# -*- coding: utf-8 -*-
"""
Created on Mon Aug  8 11:06:38 2016

@author: Caiyi Zhu

"""
import numpy as np


def load_data():
    """
    TODO:
    load and preprocess the dataset for each category, AR, AP, payroll, expense,
    The data source can be either DB, json, or local files
    """
    data_ar, data_ap, data_payroll, data_expense = [None for i in range(4)]
    return data_ar, data_ap, data_payroll, data_expense


class cashflow_rich:
    def train(self, DATAFRAME):
        self.accuracy = 90
        pass

    def predict(self, ROW):
        return np.random.uniform(low=10, high=100)




if __name__ == "__main__":
    # ---  load  and preprocess data
    data_ar, data_ap, data_payroll, data_expense = load_data()
    model = cashflow_rich()
    model.train([])
    year, month, day = 2016, 9, 1
    ar= model.predict([])
    print ar

