"""
Model  Persisention  Config

"""

import datetime


MODEL_SAVE_PATH=r'/home/dawson/framework/models/'


def genModeldFilePath(modelName):
    timestr = '{:%Y%m%d%H%M%S}'.format(datetime.datetime.now())
    return  MODEL_SAVE_PATH + timestr + "-M-" + modelName + ".pkl"