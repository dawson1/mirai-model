import  web
from qedata.mirai.models.ArModel import cash_flow_prediction
from qedata.mirai.models.ExpenseModel import cash_flow_expense
from qedata.mirai.models.PayrollModel import cash_flow_payroll
from qedata.mirai.models.AR_simple import *
from qedata.mirai.models.apSimpleModel import *
import json
from decimal import Decimal




class PayrollModelController:
    def POST(self):
        inputData = web.input()
        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        prTrainData = []
        prLabels = []
        for row in jsonArr["training"]:
            line = [row["col2"], row["col5"]]
            prLabels.append(float(row["col4"]))
            prTrainData.append(line)

        prModel=cash_flow_payroll(prTrainData ,prLabels )
        prModel.train()

        reslist = []
        plist = []
        for row in jsonArr["predict"]:
            plist.append([row["col2"], row["col5"]])

        results = prModel.predict(plist)
        idx = 0
        for row in jsonArr["predict"]:
            reslist.append({"pr_id": row["col0"], "label": round(Decimal(results[idx], 2))})
            idx += 1
        return json.dumps(reslist)


class ExpensesModelController:
    def POST(self):
        inputData = web.input()
        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        expTrainData = []
        expLabels = []
        for row in jsonArr["training"]:
            line = [ row["col2"] , row["col7"] ]
            expLabels.append( float(row["col5"]))
            expTrainData.append(line)

        expModel = cash_flow_expense( expTrainData , expLabels )
        expModel.train()

        reslist = []
        plist = []
        for row in jsonArr["predict"]:
            plist.append( [ row["col2"] , row["col7"] ])

        results = expModel.predict(plist)
        idx = 0
        for row in jsonArr["predict"]:
            reslist.append({"exp_id": row["col0"], "label": round(Decimal(results[idx], 2))})
            idx+=1
        return json.dumps(reslist)





class ArModelController:
    def POST(self):
        inputData = web.input()
        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        arTrainData = []
        arLabels = []
        for row in jsonArr["training"]:
            line = [row["col1"], row["col2"], row["col3"], row["col4"], row["col5"]]
            arTrainData.append(line)
            arLabels.append( float(row["col7"]) )

        arModel = cash_flow_prediction(arTrainData, arLabels)
        arModel.train()

        reslist = []
        plist = []
        for row in jsonArr["predict"]:
            plist.append([row["col1"], row["col2"], row["col3"], row["col4"], row["col5"]])
            #result = arModel.predict([[row["col1"], row["col2"], row["col3"], row["col4"], row["col5"]]])
            #reslist.append({"ar_id": row["col0"], "label": result})
        print "plist::",plist

        results = arModel.predict(plist)

        print  results
        idx = 0
        for row in jsonArr["predict"]:
            reslist.append({"ar_id": row["col0"], "label": results[idx]})
            idx=idx+1







        return json.dumps(reslist)



class  ArSimpleController:
    def POST(self):
        inputData = web.input()
        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        arTrainData = []
        arLabels = []
        for row in jsonArr["training"]:
            line = [ row["col1"], row["col2"], row["col3"], row["col4"], row["col5"]]
            arTrainData.append(line)
           # print line
            arLabels.append(float(row["col7"]))

        data = gen_paid_invoices(arTrainData, arLabels)
        data_sorted = sort_paid_invoices(data)
        ar_real = monthly_position(data_sorted)

        X = np.array([[int(sample[0]), int(sample[1])] for sample in ar_real])
        y = np.array([sample[2] for sample in ar_real])



        modelFake = AR_simple(X, y )
        modelFake.train()

        y_pred = modelFake.predict([   [7, 2016], [8, 2016], [9, 2016], [10, 2016], [11, 2016], [12, 2016]
                                    ,[1, 2017], [2, 2017] ,  [3, 2017] ,  [4 , 2017 ]  , [5, 2017] ,  [6, 2017] , [7, 2017]

                                    ])

        print  y_pred

        return  json.dumps({
             "position":list(y_pred)
        })


class ApSimpleController :

    def POST(self):
        inputData = web.input()
        web.header('Content-Type', 'application/json')
        data = web.data()



        jsonArr = json.loads(data)

        print jsonArr


        apTrainData = []
        apLabels = []
        for row in jsonArr["training"]:
            line = [row["col1"], row["col2"]]
            apTrainData.append(line)
            # print line
            apLabels.append(float(row["col3"]))

        APmodel = cash_flow_ap( apTrainData , apLabels )
        APmodel.train()
        #y_pred = model.predict(X_raw_predict)


        reslist = []
        plist = []
        for row in jsonArr["predict"]:
            plist.append([row["col1"], row["col2"]])
            # result = arModel.predict([[row["col1"], row["col2"], row["col3"], row["col4"], row["col5"]]])
            # reslist.append({"ar_id": row["col0"], "label": result})
        print "plist::", plist

        results = APmodel.predict(plist)

        print  results
        idx = 0
        for row in jsonArr["predict"]:
            reslist.append({"ap_id": row["col0"], "label": results[idx]})
            idx = idx + 1

        return json.dumps(reslist)












