from qedata.framework.models.PayrollModel import cash_flow_payroll
from qedata.framework.utils.ModelDataService import ModelDataUtil
import  web
import  pickle
import  json
from  SystemConfig import genModeldFilePath
from  qedata.framework.utils.AuditLogUtil import AuditLogStruct ,  AuditLog
import datetime
from decimal import Decimal


class  PRCreateAction:

    def POST(self):

        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        prTrainData = []
        prLabels = []
        for row in jsonArr["training"]:
            line = [row["col2"], row["col5"]]
            prLabels.append(float(row["col4"]))
            prTrainData.append(line)

        prModel = cash_flow_payroll(prTrainData, prLabels)
        prModel.train()


        adlog = AuditLogStruct()
        adlog.start_time = datetime.datetime.now()

        plist = []
        for row in jsonArr["predict"]:
            plist.append([row["col2"], row["col5"]])

        results = prModel.predict(plist)


        modelName = 'CASHFLOW_PR'
        userId = jsonArr["user_id"]
        adlog.user_id = userId
        mu = ModelDataUtil(None)
        log = AuditLog(mu.conn)

        arrMI = mu.qryModelbyNameUid(modelName, userId)

        if arrMI == None  or len(arrMI)==0 :
            """ create  a  exp  model """
            modelPath = genModeldFilePath(modelName)

            """store  the  model"""
            output = open(str(modelPath), 'wb')
            pickle.dump(prModel, output)
            output.close()
            newMI = [modelName, userId, modelPath, prModel.accuracy]

            mu.createModel(newMI)

            adlog.end_time = datetime.datetime.now()
            adlog.model_event = adlog.MODEL_EVENT_CREATE
            adlog.update_time = datetime.datetime.now()
            adlog.records = len(jsonArr["training"])

            log.recordAudit(adlog)
            mu.commit()

        else:

            mi = arrMI[0]

            adlog.end_time = datetime.datetime.now()

            adlog.update_time = datetime.datetime.now()
            adlog.records = len(jsonArr["training"])
            adlog.model_id = mi[0]

            if mi[6] > prModel.accuracy:
                print "debug=======3"
                """
                model is keep
                """
                adlog.model_event = adlog.MODEL_EVENT_PREDICTION
                print str(mi[10])
                input = open(str(mi[10]), 'rb')
                oModel = pickle.load(input)
                results = oModel.predict(plist)
                input.close()
            else:
                print "debug=======4"
                """
                update model file
                """
                adlog.model_event = adlog.MODEL_EVENT_REFLESH
                output = open(str(mi[10]), 'wb')
                pickle.dump(prModel, output)
                output.close()
                updateMI = [mi[0], prModel.accuracy]
                mu.updateArModel(updateMI)
            log.recordAudit(adlog)
            mu.commit()

        mu.closeConnection()

        # print  results
        idx = 0
        reslist = []
        for row in jsonArr["predict"]:
            reslist.append({"pr_id": row["col0"], "label": results[idx]})
            idx = idx + 1

        return json.dumps(reslist)


class PRPredictAction:

    def  POST(self):
            web.header('Content-Type', 'application/json')
            data = web.data()
            jsonArr = json.loads(data)

            print jsonArr

            modelName = 'CASHFLOW_PR'
            userId = jsonArr["user_id"]

            adlog = AuditLogStruct()
            adlog.start_time = datetime.datetime.now()
            adlog.user_id = userId
            adlog.model_event = adlog.MODEL_EVENT_PREDICTION

            mu = ModelDataUtil(None)

            log = AuditLog(mu.conn)

            arrMI = mu.qryModelbyNameUid(modelName, userId)
            print  arrMI
            if len(arrMI) == 0:
                adlog.model_event = adlog.MODEL_EVENT_USER_NONE
                log.recordAudit(log)
                mu.commit()
                mu.closeConnection()
                return json.dumps({
                    "success": False,
                    "message": "user expenses model has not been  created"
                })

            plist = []
            for row in jsonArr["predict"]:
                plist.append([row["col2"], row["col5"]])
            mi = arrMI[0]


            print str(mi[10])
            input = open(str(mi[10]), 'rb')
            oModel = pickle.load(input)
            results = oModel.predict(plist)
            input.close()

            adlog.end_time = datetime.datetime.now()
            adlog.update_time = datetime.datetime.now()
            adlog.model_id = mi[0]
            log.recordAudit(adlog)

            mu.commit()
            mu.closeConnection()
            idx = 0
            reslist = []
            for row in jsonArr["predict"]:
                reslist.append({"pr_id": row["col0"], "label": results[idx]})
                idx = idx + 1

            return json.dumps(reslist)



class  PrEvaluationAction:

    def POST(self):
        web.header('Content-Type', 'application/json')
        data = web.data()
        jsonArr = json.loads(data)
        modelName = 'CASHFLOW_PR'
        userId = jsonArr["user_id"]

        mu = ModelDataUtil(None)
        arrMI = mu.qryModelbyNameUid(modelName, userId)

        accurancy = 0

        print  arrMI
        prTrainData = []
        prLabels = []
        for row in jsonArr["training"]:
            line = [row["col2"], row["col5"]]
            prLabels.append(float(row["col4"]))
            prTrainData.append(line)

        prModel = cash_flow_payroll(prTrainData, prLabels)
        prModel.train()

        if arrMI == None or len(arrMI) == 0:

            """
             model is  null , framework can create a user ar model for this user
            """
            modelPath = genModeldFilePath(modelName)

            """store  the  model"""
            output = open(str(modelPath), 'wb')
            pickle.dump(prModel, output)
            output.close()
            newMI = [modelName, userId, modelPath, prModel.accuracy]

            mu.createModel(newMI)
            accurancy = prModel.accuracy
            mu.commit()
        else:

            """
            the  model is  exist . do  the  model evaluation
            """
            mi = arrMI[0]

            if mi[6] > prModel.accuracy:
                print "debug=======3"
                """
                model is keep
                """
                print str(mi[10])
                print "model accuracy::", mi[6]
                accurancy = mi[6]
            else:
                print "debug=======4"
                """
                update model file
                """
                output = open(str(mi[10]), 'wb')
                pickle.dump(prModel, output)
                output.close()
                updateMI = [mi[0], prModel.accuracy]
                mu.updateArModel(updateMI)
                mu.commit()
                accurancy = prModel.accuracy

        mu.closeConnection()
        return json.dumps({
            "success": True,
            "accuracy": accurancy
        })















