# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 15:46:48 2016

@author: Caiyi Zhu

This code is to verify that the previous integration of AR position is not correct, we should put the training and testing together to calculate the AR position.
"""

#import matplotlib.pyplot as plt


import numpy as np
#import scipy
#import matplotlib.pyplot as plt
#import calendar
#import standard
from sklearn import preprocessing
from sklearn import linear_model
import csv
#from sklearn.svm import SVR
#from sklearn.cross_validation import train_test_split

#from string_feature_extractor import string_feature_extractor

def write_csv(path, header, X):
    N = len(X)
 
    with open(path,'wb') as f:
        csv_writer = csv.writer(f, delimiter = ',')
        csv_writer.writerow(header)

        for i in range(N):
            row = list(X[i])
            csv_writer.writerow(row)
                
nb_days_month={'Jan':31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
month2int ={'Jan':1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
int2month = dict((val, key) for key,val in month2int.items())



def date2list(s):
    day, month, year = s.split('-')
    return [int(day), month2int[month], int(year)]


def load_data(path):
    X_raw, y = [], []
    
    X_raw_predict = []
    with open(path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        spamreader.next()
        for row in spamreader:
            if len(row[-1]) != 0:
                X_raw.append(row[1:6])
                y.append(float(row[-1]))
            else:
                X_raw_predict.append(row[1:6])
    return X_raw, y, X_raw_predict
    
    
def gen_paid_date(issue_date, diff_days):
    day, month, year = date2list(issue_date)
    day += int(diff_days)
    while True:
        #day += diff_days
        nb_days = nb_days_month[int2month[month]]
        if day <= nb_days:
            break
        else:
            day -= nb_days
            month += 1
            if month > 12:
                month -= 12
                year += 1
    return '-'.join([str(day), str(month), str(year)])
    
def gen_paid_invoices(X,y):
        """
        return a list a paid invoice
        generate the pending invoice and diff_days (actual or predicted), to generate paid invoices
        """        
        assert len(X) == len(y)
        res = []
        for i in range(len(X)):
            issue_date, amount, currency, company, due_date = X[i]
            diff_days = y[i]
            paid_date = gen_paid_date(issue_date, diff_days)
            res.append([issue_date, float(amount), diff_days, paid_date])
        return res    


    
def sort_paid_invoices(data):
    """
    sort the record in data,  the format of records: [issue_date, amount, diff_days, paid_date]
    """
    def my_cmp(row1, row2):
        d1, m1,y1 = [int(s) for s in row1[-1].split('-')]
        d2, m2, y2 = [int(s) for s in row2[-1].split('-')]
        if y1 < y2:
            return -1
        elif y1 > y2:
            return 1
        else:
            if m1 < m2:
                return -1
            elif m1 > m2:
                return 1
            else:
                if d1 < d2:
                    return -1
                elif d1 > d2:
                    return 1
                else: 
                    return 0

    return sorted(data, cmp = my_cmp)
    """
    get the monthly AR position
    """
def monthly_position(data):
    """
    record in data is the format: [issue_date, amount, diff_days, paid_date]
    plot the month AR position
    """
    res = []
    window = [data[0]] # contains data in a month
    for row in data[1:]:
        d1, m1, y1 = window[-1][-1].split('-')
        d2, m2, y2 = row[-1].split('-')
        if m1==m2 and y1 == y2:
            window.append(row)
        else:
            #print window
            total_amount = 0
            for tmp in window:
                total_amount += float(tmp[1])
            res.append([m1, y1, total_amount])
            
            window = [row]
    if len(window) > 0:
        d1, m1, y1 = window[-1][-1].split('-')
        total_amount = 0
        for tmp in window:
            total_amount += float(tmp[1])
        res.append([m1, y1, total_amount])
    return res

class AR_simple():
    
    def __init__(self, X_raw, y, is_one_hot = True):
        """
        Each sample in X_raw is [month, year] 
        """        
        
        # --- the label
        self.y = np.array(y)
        
        # --- the flag of one-hot encoding
        self.is_one_hot = is_one_hot
        
        # --- the model
        self.clf = linear_model.Ridge (alpha = 0.5,normalize=True) #SVR (kernel='linear', gamma=0.1, C= 1e3)
        #self.clf = SVR(kernel='linear', gamma=0.01, C= 1e4) 
        
        X = np.array(X_raw)
        if self.is_one_hot:
            self.enc =  preprocessing.OneHotEncoder(categorical_features = range(2), sparse = False, handle_unknown = 'ignore')
            X = self.enc.fit_transform(X)
        self.X = X
        
        """
        TODO: 
        """
        
        
    def feature_engineering(self, rows):
        """
        parse the data into numerical value
        example:
        row = [03-Jan-14,996.23, CAD, Loblaw Companies, 02-Feb-14]
        return [3,1,14, 996.23, 1, 1, 2, 2, 14]  # if the currency, company name is unseen in the prediction set, map to -1
        
        tested, working
        """
        
        pass

        
    def train(self):
        """
        this function can be moved to the init function
        """
        #self.accuracy = self._score()  
        
        print "model training..."
        self.clf.fit(self.X, self.y)
        
          
    def predict(self, samples_raw):
        """
        samples_raw: a list of raw samples
        return a list of predicted label
        format: [month(int), year(int)]
        """
        print "prediction for new data"
        X = np.array(samples_raw)
        if self.is_one_hot:    
            X = self.enc.transform(X)
        res = self.clf.predict(X)
        return res
    #def write(self, path, header, X):
        #write_csv(path, header, X)
    
    def good_prediction(self,y,y_hat):
        """
        Define what is a good prediction
        return true if y_hat is consider a good prediction for y
        """
        # --- return True if the prediction is within 20% error
        if (y-y_hat)/float(y) < 0.2:
            return True
        return False
    def _score(self):
        """
        calculate the performance of the model        
        """
        pass
            
        
            

if __name__ == "__main__":
    
    

                        
    
    """
    for the simple cash-flow model
    """
    path = 'dataset-ar-patty.csv'
    X_raw, y, X_raw_predict = load_data(path)


    print X_raw
    data = gen_paid_invoices(X_raw, y)
    data_sorted = sort_paid_invoices(data)
    ar_real = monthly_position(data_sorted)
    
    X = np.array([[int(sample[0]), int(sample[1])] for sample in ar_real])
    y = np.array([sample[2] for sample in ar_real])
    model = AR_simple(X, y)
    model.train()
    y_pred = model.predict([[7, 2016],[8,2016],[9,2016],[10,2016],[11,2016],[12,2016]])

    print  y_pred
    
    #plt.plot(y,color = 'b', linestyle = 'solid')
    #plt.plot(np.concatenate((y,y_pred), axis = 0),color = 'r', linestyle = 'dashed')
    #plt.show()
    
    
    
    """
    testing
    """
#    X_train, X_test, y_train, y_test = train_test_split(X,y)    
#    model = AR_simple(X_train, y_train)
#    
#    model.train()
#    y_test_pred = model.predict(X_test)
    
#    plt.plot(y_test, label = 'real')
#    plt.plot(y_test_pred, label = 'pred')
#    plt.legend()
    
    
    
            
    
