# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 15:46:48 2016

@author: Caiyi Zhu
"""

# from pylab import*
# from numpy import*
# import matplotlib.pyplot as plt

# import time
# import mlp

import numpy as np
# import scipy
# import matplotlib.pyplot as plt
# import calendar
# import standard
from sklearn import preprocessing
from sklearn import linear_model
import csv
from sklearn.svm import SVR


def write_csv(path, header, X):
    N = len(X)

    with open(path, 'wb') as f:
        csv_writer = csv.writer(f, delimiter=',')
        csv_writer.writerow(header)

        for i in range(N):
            row = list(X[i])
            csv_writer.writerow(row)


nb_days_month = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30,
                 'Oct': 31, 'Nov': 30, 'Dec': 31}
month2int = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10,
             'Nov': 11, 'Dec': 12}
int2month = dict((val, key) for key, val in month2int.items())


def date2list(s):
    day, month, year = s.split('-')
    return [int(day), month2int[month], int(year)]


def load_data(path):
    X_raw, y = [], []

    X_raw_predict = []
    with open(path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

        for row in spamreader:
            if len(row[-1]) != 0:
                X_raw.append(row[:-1])
                y.append(float(row[-1]))
            else:
                X_raw_predict.append(row[:-1])
    return X_raw, y, X_raw_predict


class cash_flow_prediction():
    def __init__(self, X_raw, y, is_one_hot=False , handle_unknown = 'ignore' ):
        # --- the label
        self.y = np.array(y)

        # --- the flag of one-hot encoding
        self.is_one_hot = is_one_hot

        # --- the model
        self.clf = linear_model.Ridge(alpha=0.5, normalize=True)  # SVR (kernel='linear', gamma=0.1, C= 1e3)
        # self.clf = SVR(kernel='linear', gamma=0.01, C= 1e4)

        # --- the features in labeled data
        X, dict_currency, dict_company = self.feature_engineering(X_raw)
        self.dict_currency = dict_currency
        self.dict_company = dict_company
        X = np.array(X)
        if self.is_one_hot:
            self.enc = preprocessing.OneHotEncoder(categorical_features=range(3) + range(4, 9), sparse=False)
            X = self.enc.fit_transform(X)
        self.X = X

    def feature_engineering(self, rows):
        """
        parse the data into numerical value
        example:
        row = [03-Jan-14,996.23, CAD, Loblaw Companies, 02-Feb-14]
        return [3,1,14, 996.23, 1, 1, 2, 2, 14]  # if the currency, company name is unseen in the prediction set, map to -1

        tested, working
        """

        res = []
        dict_currency = {'MAX_VAL': 0}
        dict_company = {'MAX_VAL': 0}
        for row in rows:
            issue_date, amount, currency, company, due_date = row
            tmp = []
            tmp += date2list(issue_date)
            tmp.append(float(amount))
            if currency not in dict_currency:
                dict_currency[currency] = dict_currency['MAX_VAL'] + 1
                dict_currency['MAX_VAL'] += 1
            tmp.append(dict_currency[currency])

            if company not in dict_company:
                dict_company[company] = dict_company['MAX_VAL'] + 1
                dict_company['MAX_VAL'] += 1
            tmp.append(dict_company[company])

            tmp += date2list(due_date)

            res.append(tmp)

        return res, dict_currency, dict_company

    def train(self):
        """
        this function can be moved to the init function
        """
        print "model training..."
        print "X::",self.X
        print "YYY::",self.y
        self.clf.fit(self.X, self.y)

        self._score()

    def predict(self, samples_raw):
        """
        samples_raw: a list of raw samples
        return a list of predicted label
        """
        print "prediction for new data"
        X = []
        for sample in samples_raw:
            issue_date, amount, currency, company, due_date = sample
            tmp = []
            tmp += date2list(issue_date)
            tmp.append(float(amount))
            if currency not in self.dict_currency:
                tmp.append(0)
            else:
                tmp.append(self.dict_currency[currency])
            if company not in self.dict_company:
                tmp.append(0)
            else:
                tmp.append(self.dict_company[company])
            tmp += date2list(due_date)

            X.append(tmp)
        X = np.array(X)
        if self.is_one_hot:
            X = self.enc.transform(X)
        res = self.clf.predict(X)
        return res
        # def write(self, path, header, X):
        # write_csv(path, header, X)

    def _score(self):
        """
        TODO:
        """
        self.accuracy = np.random.uniform(low=0.6, high=0.9)


if __name__ == "__main__":
    path = 'rich_data_AR.csv'
    X_raw, y, X_raw_predict = load_data(path)

    model = cash_flow_prediction(X_raw, y)
    model.train()
    y_pred = model.predict(X_raw_predict)
    print model.accuracy
    # score = model._score()


#    path = 'rich_data_AR.csv'
#    model = cash_flow_prediction(path, file_type = 'csv')
#    model.train(X_train, y_train)
#    y_pred = model.predict(X_test)

#    X_train = model.X_train
#    print "model.X_train", len(model.X_train)
#
#    y_train = model.y_train
#    print "model.y_train", len(model.y_train)
#    X_test = model.X_test
#    print "model.X_test", len(model.X_test)
#    y_test = model.y_test
#    print "model.y_test", len(model.y_test)
#    X_predict = model.X_predict
#    #print "model.X_predict", len(model.X_predict)
#
#
#    test_pred = model.predict(X_test) # prediction on test set
#    pred_pred = model.predict(X_predict) # prediction on predict set
#
#
#    #pred_pred = pred_pred.tolist()
#    test_pred = test_pred.tolist() # print "model.train_dates", len(model.train_dates)
#    data_shown = []
#
#    for i in range(len(model.train_dates)):
#        data_shown.append(model.train_dates[i].split('-')[::-1] + [0, model.y_train[i]] + ['train'])
#
#
#
#    print "model.test_dates", len(model.test_dates)
#    for i in range(len(model.test_dates)):
#        data_shown.append(model.test_dates[i].split('-')[::-1] + [test_pred[i], model.y_test[i]] + ['test'])
#
#    print "model.predict_dates", len(model.predict_dates)
#
#    for i in range(len(model.predict_dates)):
#        data_shown.append(model.predict_dates[i].split('-')[::-1] + [pred_pred[i], 0] + ['predict'])
#
#
#    model.write('cash_flow_AR.csv', ['year','month','day','predicted','real'], data_shown)
#
#
#     # plot actual and predicted for the test set
#    plt.plot(test_pred, color = 'r' ) #label='predicted label for test set'
#    plt.plot(y_test, color = 'g') #label='actual label for test set'
#
#
## predicted label for predict set
##    plt.plot(pred_pred, color = 'b', label = 'predicted')
#
#
### actual and predicted for train set
#
#
##    plt.plot(y_train[-26:], label = '26 actual label for test set') # actual label for test set
#
#
#
#    plt.legend()
#    plt.show()
#
#
#    count = 0
#    errorPercentage = []
#    for i in range (len(test_pred)):
#        d = np.abs(y_test[i] - test_pred[i])
#        x = d / np.abs(y_test[i])
#        errorPercentage.append(x * 100)
#        if errorPercentage[i] <= 20:
#            count += 1
#
#    print count
#    print len(errorPercentage)
#    print errorPercentage
#    print "mean", np.mean(errorPercentage)
#    print "std", np.std(errorPercentage)
#    def preprocess(rows):
#        """
#        parse the data into numerical value
#        example:
#        row = [03-Jan-14,996.23, CAD, Loblaw Companies, 02-Feb-14]
#        return [3,1,14, 996.23, 1, 1, 2, 2, 14]  # if the currency, company name is unseen in the prediction set, map to -1
#
#        tested, working
#        """
#        def date2list(s):
#            day, month, year = s.split('-')
#            return [int(day), month2int[month], int(year)]
#        res = []
#        dict_currency = {'MAX_VAL':0}
#        dict_company = {'MAX_VAL':0}
#        for row in rows:
#            issue_date, amount, currency, company, due_date = row
#            tmp = []
#            tmp += date2list(issue_date)
#            tmp.append(float(amount))
#            if currency not in dict_currency:
#                dict_currency[currency] = dict_currency['MAX_VAL'] + 1
#                dict_currency['MAX_VAL'] += 1
#            tmp.append(dict_currency[currency])
#
#            if company not in dict_company:
#                dict_company[company] = dict_company['MAX_VAL'] + 1
#                dict_company['MAX_VAL'] += 1
#            tmp.append(dict_company[company])
#
#            tmp += date2list(due_date)
#
#
#            res.append(tmp)
#        return res, dict_currency, dict_company







#    file_type = 'csv'
#    data_daily = []
#    if file_type == 'txt':
#        with open(path) as f: # path should point to a txt file
#            lines = f.readlines()
#        data_daily = [s.strip('\n').split('\t') for s in lines]
#
#    elif file_type == 'csv':
#        with open(path, 'rb') as csvfile:
#            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
#            lines = []
#            for row in spamreader:
#                data_daily.append(row[:-1])
#    res, dict_currency, dict_company = preprocess(data_daily)
#
#    issue_date, amount, currency, company, due_date = sample




