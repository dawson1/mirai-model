# -*- coding: utf-8 -*-
"""
Created on Mon Aug 15 15:34:03 2016

@author: csi-digital4
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 15:46:48 2016

@author: Caiyi Zhu
"""

#from pylab import*
#from numpy import*
#import matplotlib.pyplot as plt

#import time
#import mlp

import numpy as np
#import scipy
#import matplotlib.pyplot as plt
#import calendar
#import standard
from sklearn import preprocessing
from sklearn import linear_model
import csv
from sklearn.svm import SVR
from sklearn.cross_validation import train_test_split

#def write_csv(path, header, X):
#    N = len(X)
# 
#    with open(path,'wb') as f:
#        csv_writer = csv.writer(f, delimiter = ',')
#        csv_writer.writerow(header)
#
#        for i in range(N):
#            row = list(X[i])
#            csv_writer.writerow(row)
                
nb_days_month={'Jan':31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
month2int ={'Jan':1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
int2month = dict((val, key) for key,val in month2int.items())


#def sort_record(rows):
#    def my_cmp(r1, r2):
#        d1,m1,y1 = r1[:3]
#        d2,m2,y2 = r2[:3]
#        if y1 > y2:
#            return -1
#        elif y1 < y2:
#            return 1
#        else:
#            if m1 > m2:
#                return -1
#            elif m1 < m2:
#                return 1
#            else:
#                if d1 > d2:
#                    return -1
#                elif d1 < d2:
#                    return 1
#                else:
#                    return 0
#        return 0
#    return sorted(rows, cmp = my_cmp)[::-1]


        
#def integrate(records):
#    """
#    merge multiple transaction in a day into a single record
#    """
#    N = len(records)
#    res = []
#    window = [records[0]]
#    i = 1
#    while i < N:
#        d1, m1, y1 = window[-1][:3]
#        d2, m2, y2 = records[i][:3]
#        if d1 == d2 and m1 == m2 and y1 == y2:
#            window.append(records[i])
#        else:
#            tmp = window[0][:] # make this a copy instead of pointer
#            for j in range(1, len(window)):
#                tmp[-1] += window[j][-1]
#                
#            res.append(tmp)
#            
#            window = [records[i]]
#        i += 1
#    if len(window) != 0:
#        tmp = window[0]
#        for j in range(1, len(window)):
#            tmp[-1] += window[j][-1]
#        res.append(tmp)
#    return res
    


def date2list(s):
    print s
    day, month, year = s.split('-')
    return [int(day), month2int[month], int(year)]


def load_data(path_data, idx_feature = [4,6], idx_label = 2, idx_pending = 6):
    if type(path_data) != list:
        table = []
        with open(path_data, 'rb') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            csvreader.next()
            for row in csvreader:
                table.append(row)
    else:
        table = path_data
        
    X_raw, y = [], []
    
    X_raw_pending = []
    X_raw_predict = []

    for row in table:
        #print row
        if len(row[idx_pending]) == 0:
            X_raw_pending.append([row[idx] for idx in idx_feature])
            continue

        if len(row[idx_label]) != 0:
            X_raw.append([row[idx] for idx in idx_feature])
            y.append(float(row[idx_label]))
            
        else:
            X_raw_predict.append([row[idx] for idx in idx_feature])
            
    return X_raw, y, X_raw_predict #, X_raw_pending


class cash_flow_ap():
    """
    TODO: The records in the same date need to be integrated
    """
    def __init__(self, X_raw, y, is_one_hot = False):
        # --- the label
        self.y = np.array(y)
        
        # --- the flag of one-hot encoding
        self.is_one_hot = is_one_hot
        
        # --- the model
        self.clf = linear_model.Ridge (alpha = 0.5,normalize=True) #SVR (kernel='linear', gamma=0.1, C= 1e3)
        #self.clf = SVR(kernel='linear', gamma=0.01, C= 1e4) 
        
        # --- the features in labeled data
        X, dict_exp_type = self.feature_engineering(X_raw)
        self.dict_exp_type = dict_exp_type
        X = np.array(X)
        if self.is_one_hot:
            self.enc =  preprocessing.OneHotEncoder(categorical_features = range(3), sparse = False)
            X = self.enc.fit_transform(X)
        self.X = X
        
        
        
    def feature_engineering(self, rows):
        """
        parse the data into numerical value
        """

        res = []
        dict_exp_type = {'MAX_VAL':0}
        for row in rows:
            exp_type, pay_date = row
            tmp = []
            if exp_type not in dict_exp_type:
                dict_exp_type[exp_type] = dict_exp_type['MAX_VAL'] + 1
                dict_exp_type['MAX_VAL'] += 1
            tmp.append(dict_exp_type[exp_type])
            
            
            tmp += date2list(pay_date)
            
            res.append(tmp)
            
        return res, dict_exp_type  
    
    def train(self):
        """
        this function can be moved to the init function
        """
        self.accuracy = self._score()          
                
        print "model training..."
        self.clf.fit(self.X, self.y)
        
    def predict(self, samples_raw):
        """
        samples_raw: a list of raw samples
        return a list of predicted label
        """
        print "prediction for new data"
        X = []
        for sample in samples_raw:
            exp_type, pay_date = sample
            tmp = []

            if exp_type not in self.dict_exp_type:
                tmp.append(0)
            else:
                tmp.append(self.dict_exp_type[exp_type])
            tmp += date2list(pay_date)
            
            X.append(tmp)
        X = np.array(X)
        if self.is_one_hot:    
            X = self.enc.transform(X)
        res = self.clf.predict(X)
        return res
    #def write(self, path, header, X):
        #write_csv(path, header, X)
        
    def good_prediction(self,y,y_hat):
        """
        return true if y_hat is consider a good prediction for y
        """
        # --- return True if the prediction is within 20% error
        if (y-y_hat)/float(y) < 0.2:
            return True
        return False
        
    def _score(self):
        """
        calculate the accuracy of the model
        """
        #self.accuracy = np.random.uniform(low = 0.6, high = 0.9)
        X_train, X_test, y_train, y_test = train_test_split(self.X, self.y)
        self.clf.fit(X_train, y_train)
        y_pred = self.clf.predict(X_test)
        
        #print y_test
        # --- plot the y_test, y_split
#        plt.plot(y_test[:100],label = 'real value')
#        plt.plot(y_pred[:100], label = 'predicted value')
#        plt.legend()
        
        # --- calculate the 
        nb_correct = 0
        for i in range(len(y_pred)):
            if self.good_prediction(y_pred[i], y_test[i]):
                nb_correct += 1
        return float(nb_correct)/len(y_pred)
        
        
if __name__ == "__main__":
    """
    column_names used: [product_type, paid_date]
    
    Instruction:
    To get the X_raw, y, X_raw_predict, you can either stitch the database yourself, or just call the load_data function, the input is the table similar to the CSV file
    """
    path_data = 'dataset-ap-canuck.csv'
    table = []
    with open(path_data, 'rb') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        csvreader.next()
        for row in csvreader:
            table.append(row)
    X_raw, y, X_raw_predict= load_data(table)
    
    print "X_raw",X_raw
    
    model = cash_flow_ap(X_raw, y)
    model.train()
    y_pred = model.predict(X_raw_predict)
    print  y_pred
    print model.accuracy

    

    
            
            
    
