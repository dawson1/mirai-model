from pylab import *
from numpy import *
import matplotlib.pyplot as plt

import time
import mlp

import numpy as np
# import scipy
import matplotlib.pyplot as plt
import calendar
# import standard
from sklearn import preprocessing
from sklearn import linear_model
import csv
from sklearn.svm import SVR


def write_csv(path, header, X):
    N = len(X)

    with open(path, 'wb') as f:
        csv_writer = csv.writer(f, delimiter=',')
        csv_writer.writerow(header)

        for i in range(N):
            row = list(X[i])
            csv_writer.writerow(row)


nb_days_month = {'Jan': 31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30,
                 'Oct': 31, 'Nov': 30, 'Dec': 31}
month2int = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10,
             'Nov': 11, 'Dec': 12}
int2month = dict((val, key) for key, val in month2int.items())


class cash_flow_prediction():
    def __init__(self, train_data):
        self.clf = linear_model.Ridge(alpha=0.5, normalize=True)  # SVR (kernel='linear', gamma=0.1, C= 1e3)
        self.test_dates = None
        self._preprocessing(train_data)



    def _preprocessing(self, modeldata):
        data_daily = modeldata
        """
        if file_type == 'txt':
            with open(path) as f:  # path should point to a txt file
                lines = f.readlines()
            data_daily = [s.strip('\n').split('\t') for s in lines]

        elif file_type == 'csv':
            with open(path, 'rb') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                lines = []
                for row in spamreader:
                    data_daily.append(row)
                    # print "::", data_daily
        """
        print  data_daily

        numerical_feature_ind_set = {3}
        text_feature_map = {};
        num_class_text_feature = {};  # number of classes to map categorical features to numerical values
        list_train = []
        label = []
        date = []
        list_pending = []
        ##################################

        # seperate features and label and determine train_test and predict sets
        for i in range(len(data_daily)):
            if len(data_daily[i][4].strip()) == 0:
                list_pending.append(data_daily[i][:])
                continue
            list_train.append(data_daily[i][:4])
            label.append(data_daily[i][4])

        # extract three parts of the date
        for i in range(len(list_train)):
            tmp3 = list_train[i][0].split('-')
            date.append([str(tmp3[0]), str(month2int[tmp3[1]]), str(tmp3[2])])

        # extract three rest features, ammount, currency and client name
        ammount_currecy_client = []
        for i in range(len(list_train)):
            ammount_currecy_client.append(list_train[i][1:4])

        # generate dates for train and test set and data
        data = []
        issue_date_paid = []
        for i in range(len(list_train)):
            issue_date_paid.append(str(list_train[i][0]))
            data_tmp = list_train[i][0].split('-') + ammount_currecy_client[i][:]
            data.append(data_tmp)
        # print "issue date", issue_date_paid




        ##################################### predict set




        # extract three rest features, ammount, currency and client name
        ammount_currecy_client_pending = []
        for i in range(len(list_pending)):
            ammount_currecy_client_pending.append(list_pending[i][1:4])

        # pending transaction to be used as predict set, list pending is prediction set
        issue_date_pending = []
        data_predict = []
        for i in range(len(list_pending)):
            issue_date_pending.append(str(list_pending[i][0]))
            data_tmp_pending = list_pending[i][0].split('-') + ammount_currecy_client_pending[i][:]
            data_predict.append(data_tmp_pending)

        # print "data_predict", data_predict








        ####################################################



        ## change categorical to numeric feature

        for i in range(len(data)):
            for j in range(len(data[i])):

                if j in numerical_feature_ind_set:
                    data[i][j] = float(data[i][j])

                else:

                    if j not in text_feature_map:
                        text_feature_map[j] = {}
                        num_class_text_feature[j] = 0
                    # print "data_daily", data_daily[i][j]
                    #                        print "text_feature_map[j]", text_feature_map[j]
                    if data[i][j] not in text_feature_map[j]:
                        num_class_text_feature[j] += 1;
                        text_feature_map[j][data[i][j]] = num_class_text_feature[j]
                    data[i][j] = text_feature_map[j][data[i][j]]

                    #        print "data_daily", processed_data
                    #        print "text_feature_map", text_feature_map
                    #        print "num_class_text_feature", num_class_text_feature




                    ################################################ predict set



                    ## change categorical to numeric feature

        for i in range(len(data_predict)):
            for j in range(len(data_predict[i])):

                if j in numerical_feature_ind_set:
                    data_predict[i][j] = float(data_predict[i][j])

                else:

                    if j not in text_feature_map:
                        text_feature_map[j] = {}
                        num_class_text_feature[j] = 0
                    # print "data_daily", data_daily[i][j]
                    #                        print "text_feature_map[j]", text_feature_map[j]
                    if data_predict[i][j] not in text_feature_map[j]:
                        num_class_text_feature[j] += 1;
                        text_feature_map[j][data_predict[i][j]] = num_class_text_feature[j]
                    data_predict[i][j] = text_feature_map[j][data_predict[i][j]]



                    #################################################
                    #        print "data_predict", data_predict

                    # find predict set
                    #        N = 52 # predict for next year
                    #        date_test_string = []
                    #        date_test_list = []
                    #        for i in range(N):
                    #            date_test_list.append(start_this_week)
                    #            #date_test_string.append(list2string_date(start_this_week))
                    #
                    #            #start_this_week = next_week_start(start_this_week)
                    #        X_predict = date_test_list

        X_test = data[-26:]
        test_left = data[:-26]
        X_train = test_left

        test_date = issue_date_paid[-26:]
        test_date_left = issue_date_paid[:-26]
        train_date = test_date_left

        # --- change the type of label from string to float

        label = [float(s) for s in label]

        y_test = label[-26:]
        y_test_left = label[:-26]
        y_train = y_test_left

        # print "train:", X_train # 7 years 2009-2015 for train
        # print "test: ", X_test # last 6 months as test set

        #
        #        #print "predict: ", predict_set # next year as predict set
        #
        #
        #        # change weekly represenration from day, month, year to index, year
        #        X_train_new = np.delete(X_train, 3, 1)
        #        X_test_new = np.delete(X_test, 3, 1)
        #
        #
        #
        #
        #
        #
        #        #print "train ", X_train_new
        #        #print "test ", X_test_new
        #
        #        enc = preprocessing.OneHotEncoder()
        #        enc.fit(np.array(X_train_new + X_test_new))
        #
        #        X_test_enc = enc.transform(np.array(X_test_new)) # all left for train set
        #        X_train_enc = enc.transform(np.array(X_train_new)) # last 6 months
        #
        #        X_train_enc
        #        #X_predict_enc = enc.transform(np.array(predict_set)) # last year
        #
        #
        #        X_train_enc = X_train_enc.toarray()
        #        X_test_enc = X_test_enc.toarray()
        #        test_dates
        #
        #        #X_predict_enc = X_predict_enc.toarray()




        #        import standard
        #        sd = standard.standard(X_train, X_test, X_test)
        #        sd.standardize()

        self.X_train = np.array(X_train)
        self.X_test = np.array(X_test)

        self.y_train = np.array(y_train)  # .reshape((len(y_train),1))
        self.y_test = np.array(y_test)  # .reshape((len(y_test),1))

        self.X_predict = np.array(data_predict)

        self.train_dates = train_date
        self.test_dates = test_date

        self.predict_dates = issue_date_pending

    def train(self):
        self.clf.fit(self.X_train, self.y_train)

    def predict(self, X):
        res = self.clf.predict(X)
        return res

    def write(self, path, header, X):
        write_csv(path, header, X)


# print "model.predict_dates", len(model.predict_dates)
# list_pending
#    for i in range(len(model.predict_dates)):
#        data_shown.append(model.predict_dates[i].split('-')[::-1] + [pred_pred[i], 0])
if __name__ == "__main__":
    path = 'data_AR.csv'
    model = cash_flow_prediction(path, file_type='csv')
    model.train()

    X_train = model.X_train
    print "model.X_train", len(model.X_train)
    y_train = model.y_train
    print "model.y_train", len(model.y_train)
    X_test = model.X_test
    print "model.X_test", len(model.X_test)
    y_test = model.y_test
    print "model.y_test", len(model.y_test)
    X_predict = model.X_predict
    # print "model.X_predict", len(model.X_predict)


    test_pred = model.predict(X_test)  # prediction on test set
    pred_pred = model.predict(X_predict)  # prediction on predict set

    # pred_pred = pred_pred.tolist()
    test_pred = test_pred.tolist()
    data_shown = []
    # print "model.train_dates", len(model.train_dates)
    for i in range(len(model.train_dates)):
        data_shown.append(model.train_dates[i].split('-')[::-1] + [0, model.y_train[i]] + ['train'])

    print "model.test_dates", len(model.test_dates)
    for i in range(len(model.test_dates)):
        data_shown.append(model.test_dates[i].split('-')[::-1] + [test_pred[i], model.y_test[i]] + ['test'])

    print "model.predict_dates", len(model.predict_dates)

    for i in range(len(model.predict_dates)):
        data_shown.append(model.predict_dates[i].split('-')[::-1] + [pred_pred[i], 0] + ['predict'])

    model.write('cash_flow_train_test.csv', ['year', 'month', 'day', 'predicted', 'real'], data_shown)

    # plot actual and predicted for the test set
    plt.plot(test_pred, color='r', label='predicted label for test set')
    plt.plot(y_test, color='g', label='actual label for test set')

    #    plt.plot(y_pred, label = 'predicted') # predicted label for predict set
    #    plt.plot(y_train[-26:], label = 'actual label for test set') # actual label for test set

    plt.legend()
    plt.show()

    count = 0
    errorPercentage = []
    for i in range(len(test_pred)):
        d = np.abs(y_test[i] - test_pred[i])
        x = d / np.abs(y_test[i])
        errorPercentage.append(x * 100)
        if errorPercentage[i] <= 20:
            count += 1

    print count
    print len(errorPercentage)
    print errorPercentage
    print "mean", np.mean(errorPercentage)
    print "std", np.std(errorPercentage)

