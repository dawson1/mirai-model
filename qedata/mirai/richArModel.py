"""

Create by Dawson

"""

import web
import json
import numpy as np
from  FakeRicherDataModel import  cashflow_rich
from decimal import Decimal


class prModel:

    def POST(self):
        web.header('Content-Type', 'application/json')
        data = web.data()
        print  data
        reslist = []
        jsonArr = json.loads(data)
        for row in jsonArr["predict"]:
            reslist.append({"pr_id": row["col0"], "label": round(Decimal(np.random.uniform(low=100, high=1000), 2))})
        return json.dumps(reslist)


class expensesModel:
    def POST(self):
        web.header('Content-Type', 'application/json')
        data = web.data()
        print  data
        reslist = []
        jsonArr = json.loads(data)
        for row in jsonArr["predict"]:
            reslist.append({"exp_id" : row["col0"] , "label": round(  Decimal( np.random.uniform(low=100, high=1000) , 2)  ) })
        return  json.dumps(reslist)

class arModel:


    def POST(self):
        web.header('Content-Type', 'application/json')

        data = web.data()

        print  data

        jsonArr = json.loads(data)
        arTrainData = []
        for row in jsonArr["training"] :
            line = [row["col1"] , row["col2"] , row["col3"] , row["col4"] , row["col5"] , row["col6"] , row["col7"] ]
            arTrainData.append(line)
        #print arTrainData
        arModel = cashflow_rich();
        arModel.train(arTrainData)

        #prediction

        reslist = []
        for row in jsonArr["predict"]:
            result=arModel.predict( [row["col1"] , row["col2"] , row["col3"] , row["col4"] , row["col5"]])
            reslist.append({ "ar_id" : row["col0"] ,  "label" : result  })



        return  json.dumps( reslist )