"""

process  the  AR  data

"""

import web
import json
from richerdatamodel import cash_flow_prediction
import numpy as np

class ArDataModelController:

    def POST(self):

        inputData=web.input()
        web.header("text/json")
        if inputData['trainset'] == None :
            return json.dump({
                "status":"false",
                "message":"input data is null"
            })
        jsonData=json.load(inputData)
        ArData=list()
        for row in jsonData :
            ArData.append(row)

        model = cash_flow_prediction( np.asarray(ArData) )
        model.train()





