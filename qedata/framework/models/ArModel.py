# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 15:46:48 2016

@author: Caiyi Zhu
"""

#import matplotlib.pyplot as plt


import numpy as np
#import scipy
#import matplotlib.pyplot as plt
#import calendar
#import standard
from sklearn import preprocessing
from sklearn import linear_model
import csv
from sklearn.svm import SVR
from sklearn.cross_validation import train_test_split

def write_csv(path, header, X):
    N = len(X)
 
    with open(path,'wb') as f:
        csv_writer = csv.writer(f, delimiter = ',')
        csv_writer.writerow(header)

        for i in range(N):
            row = list(X[i])
            csv_writer.writerow(row)
                
nb_days_month={'Jan':31, 'Feb': 28, 'Mar': 31, 'Apr': 30, 'May': 31, 'Jun': 30, 'Jul': 31, 'Aug': 31, 'Sep': 30, 'Oct': 31, 'Nov': 30, 'Dec': 31}
month2int ={'Jan':1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
int2month = dict((val, key) for key,val in month2int.items())



def date2list(s):
    day, month, year = s.split('-')
    return [int(day), month2int[month], int(year)]


def load_data(path):
    X_raw, y = [], []
    
    X_raw_predict = []
    with open(path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        spamreader.next()
        for row in spamreader:
            if len(row[-1]) != 0:
                X_raw.append(row[1:6])
                y.append(float(row[-1]))
            else:
                X_raw_predict.append(row[1:6])
    return X_raw, y, X_raw_predict

class cash_flow_AR():
    
    def __init__(self, X_raw, y, is_one_hot = True):
        # --- the label
        self.y = np.array(y)
        
        # --- the flag of one-hot encoding
        self.is_one_hot = is_one_hot
        
        # --- the model
        self.clf = linear_model.Ridge (alpha = 0.5,normalize=True) #SVR (kernel='linear', gamma=0.1, C= 1e3)
        #self.clf = SVR(kernel='linear', gamma=0.01, C= 1e4) 
        
        # --- the features in labeled data
        X, dict_currency, dict_company = self.feature_engineering(X_raw)
        self.dict_currency = dict_currency
        self.dict_company = dict_company
        X = np.array(X)
        if self.is_one_hot:
            self.enc =  preprocessing.OneHotEncoder(categorical_features = range(2) + range(4,8), sparse = False, handle_unknown = 'ignore')
            X = self.enc.fit_transform(X)
        self.X = X
        
        """
        TODO: 
        """
        
        
    def feature_engineering(self, rows):
        """
        parse the data into numerical value
        example:
        row = [03-Jan-14,996.23, CAD, Loblaw Companies, 02-Feb-14]
        return [3,1,14, 996.23, 1, 1, 2, 2, 14]  # if the currency, company name is unseen in the prediction set, map to -1
        
        tested, working
        """

        res = []
        dict_currency = {'MAX_VAL':0}
        dict_company = {'MAX_VAL':0}
        for row in rows:
            #print row
            issue_date, amount, currency, company, due_date = row
            tmp = []
            tmp += date2list(issue_date)
            tmp.append(float(amount))
            if currency not in dict_currency:
                dict_currency[currency] = dict_currency['MAX_VAL'] + 1
                dict_currency['MAX_VAL'] += 1
            tmp.append(dict_currency[currency])
            
            if company not in dict_company:
                dict_company[company] = dict_company['MAX_VAL'] + 1
                dict_company['MAX_VAL'] += 1
            tmp.append(dict_company[company])
            
            tmp += date2list(due_date)
            
            res.append(tmp)
            
        return res, dict_currency, dict_company   
    
        
    def train(self):
        """
        this function can be moved to the init function
        """
        self.accuracy = self._score()  
        
        print "model training..."
        self.clf.fit(self.X, self.y)
        
          
    def predict(self, samples_raw):
        """
        samples_raw: a list of raw samples
        return a list of predicted label
        """
        print "prediction for new data"
        X = []
        for sample in samples_raw:
            issue_date, amount, currency, company, due_date = sample
            tmp = []
            tmp += date2list(issue_date)
            tmp.append(float(amount))
            if currency not in self.dict_currency:
                tmp.append(1)
            else:
                tmp.append(self.dict_currency[currency])
            if company not in self.dict_company:
                tmp.append(1)
            else:
                tmp.append(self.dict_company[company])
            tmp += date2list(due_date)
            
            X.append(tmp)
        X = np.array(X)
        if self.is_one_hot:    
            X = self.enc.transform(X)
        res = self.clf.predict(X)
        return res
    #def write(self, path, header, X):
        #write_csv(path, header, X)
    
    def good_prediction(self,y,y_hat):
        """
        return true if y_hat is consider a good prediction for y
        """
        # --- return True if the prediction is within 20% error
        if (y-y_hat)/float(y) < 0.2:
            return True
        return False
    def _score(self):
        """
        ###TODO: Done
        """
        #self.accuracy = np.random.uniform(low = 0.6, high = 0.9)
        X_train, X_test, y_train, y_test = train_test_split(self.X, self.y)
        self.clf.fit(X_train, y_train)
        y_pred = self.clf.predict(X_test)
        
        
        # --- plot the y_test, y_split
        #plt.plot(y_test[:50],label = 'real value')
        #plt.plot(y_pred[:50], label = 'predicted value')
        #plt.legend()
        
        # --- calculate the 
        nb_correct = 0
        for i in range(len(y_pred)):
            if self.good_prediction(y_pred[i], y_test[i]):
                nb_correct += 1
        return float(nb_correct)/len(y_pred)
            
        
            

if __name__ == "__main__":
    
    path = 'ar.csv'
    X_raw, y, X_raw_predict = load_data(path)
    
    model = cash_flow_AR(X_raw, y)
    model.train()
    y_pred = model.predict(X_raw_predict)
    print "model accuracy", model.accuracy




    
            
            
    
