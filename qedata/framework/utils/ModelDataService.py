import cx_Oracle
import OracleConfig

class ModelDataUtil:



    def __init__(self ,  conn ):
        self.dns = cx_Oracle.makedsn(OracleConfig.ora_ipAddress, OracleConfig.ora_port, OracleConfig.ora_sid)

        if conn == None :
            self.conn = cx_Oracle.connect(OracleConfig.ora_username, OracleConfig.ora_passwd, self.dns)
        else :
            self.conn = conn


    def updateArModel(self ,  params ):
        conn = self.conn
        cur = conn.cursor()
        ModelSQL = "UPDATE  MODEL_INFO   SET BEST_SCORE=:best_score WHERE MODEL_ID=:model_id"
        cur.prepare(ModelSQL)
        result = cur.execute(None, {
            "model_id": params[0],
            "best_score": params[1]
        })
        return 0



    def qryModelbyNameUid(self ,  modelName , userId):
        conn = self.conn
        cur = conn.cursor()
        print  "qrymodel ::" , modelName , userId
        ModelSQL = "SELECT * FROM MODEL_INFO WHERE MODEL_NAME =:model_name AND USER_ID =:user_id"
        cur.prepare(ModelSQL)
        result = cur.execute(None ,  {
            "user_id" : userId ,
            "model_name" :modelName
        } )
        arr= result.fetchall()
        print  arr
        return  arr

    def createModel(self, params):
        conn = self.conn
        cur = conn.cursor()

        """
        ['CASHFLOW_AR', u'8a80cb81565c679301565c679a2b0000', '/home/changfeng/develop/datafiles/models/20160829153626-M-CASHFLOW_AR.pkl', 0.8545454545454545]
        """

        insertSQL = "INSERT INTO MODEL_INFO ( MODEL_ID , MODEL_NAME , USER_ID , BEST_SCORE , MODEL_PATH , UPDATE_TIME) VALUES (" \
                    "  SYS_GUID() , :model_name , :user_id ,  :best_score  ,   :model_path  ,   SYSDATE )  "
        cur.prepare(insertSQL)
        cur.execute(None ,{
            "model_name" : params[0] ,
            "user_id" : params[1],
            "best_score" :  params[3] ,
            "model_path" : params[2]
        })
        return 0

    def closeConnection(self):
        self.conn.close()


    def commit(self):
        self.conn.commit()







if __name__ == '__main__':
     mu = ModelDataUtil()
     mu.qryModelbyNameUid()
