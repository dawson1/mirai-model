import cx_Oracle
import OracleConfig


"""
    Payroll data set
"""
class  PayrollDataService:
    def __init__(self):
        self.dns = cx_Oracle.makedsn(OracleConfig.ora_ipAddress, OracleConfig.ora_port, OracleConfig.ora_sid)
        self.conn = cx_Oracle.connect(OracleConfig.ora_username, OracleConfig.ora_passwd, self.dns)

    def qryTrainAndTestPoint(self, user_id):
        conn = self.conn
        cur = conn.cursor()
        ttSQL = "SELECT   MAX(  PR.PAY_DATE  )   ,   ADD_MONTHS(  MAX(  PR.PAY_DATE  ) , -6 )      FROM  USER_PAYROLL  PR " \
                " WHERE   PR.SALARY IS NOT NULL    AND   USER_ID = :user_id   GROUP BY   USER_ID";

        cur.prepare(ttSQL)
        result = cur.execute(None, {
            "user_id": user_id
        })

        arr = result.fetchall()

        return  arr[0]


    def  qryTATDataset(self, user_id , params):
        conn = self.conn
        cur = conn.cursor()
        trainSQL = "SELECT  *  FROM   USER_PAYROLL    PR   WHERE  " \
                "  PR.SALARY IS  NOT   NULL   AND   USER_ID =:user_id  AND " \
                "   PR.PAY_DATE  <=  :sp_date  "

        cur.prepare(trainSQL)
        arrTrain = cur.execute(None, {
            "user_id": user_id,
            "sp_date": params[1]
        }).fetchall()

        testSQL = "SELECT  *  FROM   USER_PAYROLL    PR   WHERE   " \
                  "  PR.SALARY IS  NOT   NULL   AND   USER_ID = :user   AND    PR.PAY_DATE > :sp_date "


        arrTest = cur.execute( None , {
            "user_id" : user_id ,
            "sp_date" : params[1]
        }).fetchall()




        return  arrTrain , arrTest






    def closeConnection(self):
        self.conn.close()









class  ExpDataService :
    def __init__(self):
        self.dns = cx_Oracle.makedsn(OracleConfig.ora_ipAddress, OracleConfig.ora_port, OracleConfig.ora_sid)
        self.conn = cx_Oracle.connect(OracleConfig.ora_username, OracleConfig.ora_passwd, self.dns)

    def qryTrainAndTestPoint(self,user_id):
        conn = self.conn
        cur = conn.cursor()

        #arr = result.fetchall()





        return


    def closeConnection(self):
        self.conn.close()



class  ArDataService :
    def  __init__(self , conn):
        self.dns = cx_Oracle.makedsn(OracleConfig.ora_ipAddress, OracleConfig.ora_port, OracleConfig.ora_sid)
        if conn== None :
            self.conn = cx_Oracle.connect(OracleConfig.ora_username, OracleConfig.ora_passwd, self.dns)
        else:
            self.conn = conn

    def  qryTrainingDataByUserId(self):
        sql="select  * from user_ar  " \
            "    where DIFF_DAYS is null and   user_id=:user_id"
        conn = self.conn
        cur = conn.cursor()
        cur.prepare(sql)
        result = cur.execute(None,{
             "user_id" : '8a80cb81565c679301565c679a2b0003'
        })
        arr = result.fetchall()
        cur.close()
        return  arr

    def qryTrainAndTestPoint(self ,  user_id ):
        conn = self.conn
        cur = conn.cursor()
        ttSQL = "select max( issue_date ) end_date, " \
                " add_months( max( issue_date )  , -6 ) ps, min( issue_date ) start_date " \
                " from  user_ar  where  DIFF_DAYS is not null  " \
                " and  user_id=:user_id  group by user_id"
        cur.prepare(ttSQL)
        result = cur.execute(None, {
            "user_id": user_id
        })
        pointArr = result.fetchall()
        cur.close()
        return  pointArr[0]

    def qryTrainATestDataset(self,  user_id ,  params ):
        conn = self.conn
        cur = conn.cursor()
        trainSQL = " select     AR.AR_ID ,  AR.INVOICE_NO , TO_CHAR( AR.ISSUE_DATE  , 'dd-Mon-yy')   , AR.AMOUNT , AR.CURRENCY , AR.COMPANY_NAME , " \
                   " TO_CHAR( AR.DUE_DATE , 'dd-Mon-yy') , AR.STATUS, AR.DIFF_DAYS ,  AR.USER_ID " \
                   "from  user_ar  AR  where  AR.DIFF_DAYS is not null  " \
                   "and  AR.user_id =:user_id and  AR.issue_date < :issue_date  "
        cur.prepare(trainSQL)
        arrTrain = cur.execute(None, {
            "user_id": user_id ,
            "issue_date": params[1]
        }).fetchall()

        testSQL = " select   AR.AR_ID ,  AR.INVOICE_NO , TO_CHAR( AR.ISSUE_DATE  , 'dd-Mon-yy')   , AR.AMOUNT , AR.CURRENCY , AR.COMPANY_NAME , " \
                  " TO_CHAR( AR.DUE_DATE , 'dd-Mon-yy') , AR.STATUS, AR.DIFF_DAYS ,  AR.USER_ID from  user_ar AR  where  AR.DIFF_DAYS is not null  " \
                   "and  AR.user_id =:user_id and  AR.issue_date >= :issue_date"
        cur.prepare(testSQL)
        arrTest = cur.execute(None, {
            "user_id": user_id,
            "issue_date": params[1]
        }).fetchall()

        cur.close()

        return   arrTrain , arrTest

    def  insertPredictionArResult(self ,  params):
        conn = self.conn
        cur = conn.cursor()

        """
        AR_ID        NOT NULL VARCHAR2(64)
MODEL_ID              VARCHAR2(64)
INVOICE_NO            VARCHAR2(64)
USER_ID               VARCHAR2(64)
ISSUE_DATE            DATE
AMOUNT                NUMBER(15,4)
CURRENCY              VARCHAR2(4)
COMPANY_NAME          VARCHAR2(64)
DUE_DATE              DATE
PAID_DATE             DATE
STATUS                VARCHAR2(32)
DIFF_DAYS             NUMBER(6)
ACT_AR_ID             VARCHAR2(64)
TEST_FLAG             VARCHAR2(2)
ACT_PAIDDATE          DATE

        """


        insertSQL = "INSERT INTO PREDICTION_AR  ( AR_ID , INVOICE_NO ,ISSUE_DATE ,AMOUNT ,  CURRENCY ,  COMPANY_NAME ,  PAID_DATE ,  STATUS ,  DIFF_DAYS ,  USER_ID ," \
                    "  ACT_AR_ID ,    ACT_PAIDDATE  ,  TEST_FLAG ) VALUES " \
                    "(  SYS_GUID() , :1 ,  TO_DATE( :2  ,'dd-Mon-yy') ,  :3 ,  :4 ,   :5 ,  TO_DATE( :6 ,'dd-Mon-yy') + TO_NUMBER(:7)  , :8 , :9 , :10 , :11,  TO_DATE( :12 ,'dd-Mon-yy') , :13)"

       # 'INV-0000001', '04-Jan-14', 26361.27, 'CAD', 'Jean Coutu Group', '04-Jan-14', 'PAID', 10, 10, '8a80cb81565c679301565c679a2b0000', '3AE86D21FDE60D65E053020011AC3AFC', '03-Feb-14'
        print  insertSQL

        cur.bindarraysize = len(params)
        #cur.setinputsizes(int, 20)
        cur.executemany( insertSQL ,  params )




        cur.close()
        return 0




    def closeConnection(self):
        self.conn.close()








if __name__ == '__main__':


        ds = ArDataService()

        tt = ds.qryTrainAndTestPoint("8a80cb81565c679301565c679a2b0003")
        print tt[1]

        arrTrain , arrTest = ds.qryTrainATestDataset( "8a80cb81565c679301565c679a2b0003" , tt )

        print  arrTrain , arrTest
        ds.closeConnection()

        userId = '8a80cb81565c679301565c679a2b0005'

        dsPr = PayrollDataService()

        print  dsPr.qryTrainAndTestPoint(userId)


        dsPr.closeConnection()

