import cx_Oracle
import OracleConfig




class AuditLogStruct:

    MODEL_EVENT_CREATE="CREATE"
    MODEL_EVENT_REFLESH="REFLESH"
    MODEL_EVENT_PREDICTION="PREDICT"
    MODEL_EVENT_USER_NONE="NONE"

    def __init__(self):
        self.model_id=""
        self.user_id=""
        self.start_time=None
        self.end_time = None
        self.message = ""
        self.update_time=None
        self.records=0
        self.model_event=""




class  AuditLog :

    def __init__(self, conn):
        self.dns = cx_Oracle.makedsn(OracleConfig.ora_ipAddress, OracleConfig.ora_port, OracleConfig.ora_sid)

        if conn == None:
            self.conn = cx_Oracle.connect(OracleConfig.ora_username, OracleConfig.ora_passwd, self.dns)
        else:
            self.conn = conn

    def  recordAudit(self, params):
        conn = self.conn
        cur = conn.cursor()
        insertSQL = "INSERT INTO  MODEL_AUDITLOG   " \
                    "VALUES ( SYS_GUID()  , :model_id ,  :user_id  ,    :model_event ,  :start_time ,:end_time ,  :message ,   :update_time ,  :records    )"

        cur.prepare(insertSQL)
        """
        cur.execute(None, {
            "model_id":  params.model_id,
            "user_id": params.user_id,
            "model_event": params.model_event,
            "start_time": params.start_time,
            "end_time": params.end_time ,
            "message" : params.message ,
            "update_time" :  params.update_time ,
            "records": 12
            #"run_time" : params.end_time - params.start_time

        })
        """
        return 0
